package No_3;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class No_3 {

    NumberFormat format = new DecimalFormat("#0.00");

    public int operasiHitung(int angka1, int angka2){
        return angka1+angka2;
    }

    public float operasiHitung(float angka3, float angka4){
        return angka3 - angka4;
    }

    public String operasiHitung(double angka5, double angka6){
        return format.format(angka5*angka6);
    }

    public String operasiHitung(double angka7, float angka8){
        return format.format(angka7/angka8);
    }

    public static void main(String[] args) {
        No_3 hitungan = new No_3();
        System.out.println("Hasil penjumlahan : "+hitungan.operasiHitung(4,5));
        System.out.println("Hasil pengurangan : "+hitungan.operasiHitung(7f,6f));
        System.out.println("Hasil perkalian : "+hitungan.operasiHitung(6.5, 5.7));
        System.out.println("Hasil pembagian : "+hitungan.operasiHitung(6.7, 8f));
    }
}
