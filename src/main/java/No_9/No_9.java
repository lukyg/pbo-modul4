package No_9;

public class No_9 {

    private int kuadrat(int nilai){
        return nilai*nilai;
    }

    private double maksimum(float angka1, double angka2){
        if(angka1>angka2) return angka1;
        else return angka2;
    }

    private double minimum(double angka3, double angka4){
        if(angka3<angka4) return angka3;
        else return angka4;
    }

    public static void main(String[] args) {
        No_9 input = new No_9();
        System.out.println("Hasil kuadrat : "+input.kuadrat(5));
        System.out.println("Nilai maksimal : "+input.maksimum(5.7f, 8.7));
        System.out.println("Nilai minimum : "+input.minimum(5.6, 6.8));
    }
}
