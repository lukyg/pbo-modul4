package No_7;

public class No_7 {

    int panjang, lebar, sisi;
    double tinggi, alas;

    No_7(int x) {
        sisi = x;
    }
    No_7(int p, int l){
        panjang = p;
        lebar = l;
    }
    No_7(double a, double t){
        tinggi = t;
        alas= a;
    }

    int hitungPersegiPanjang(){
        return (panjang*lebar);
    }

    int hitungPersegi(){
        return (sisi*sisi);
    }
    double hitungSegitiga(){
        return (alas*tinggi*0.5);
    }


    public static void main(String[] args) {
        No_7 angka1 = new No_7(5);
        No_7 angka2 = new No_7(6,5);
        No_7 angka3 = new No_7(5.4, 4.5);
        System.out.println("Hasil luas persegi : "+angka1.hitungPersegi());
        System.out.println("Hasil luas persegi panjang : "+angka2.hitungPersegiPanjang());
        System.out.println("Hasil luas segitiga : "+angka3.hitungSegitiga());
    }

}
