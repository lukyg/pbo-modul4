package No_6;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class No_6 {

    NumberFormat format = new DecimalFormat("#0.00");
    public int bangunRuang(int sisi){
        return sisi*sisi;
    }
    public int bangunRuang(int panjang, int lebar){
        return panjang*lebar;
    }

    public String bangunRuang(double alas, double tinggi){
        return format.format(alas*tinggi*0.5);
    }

    public static void main(String[] args) {
        No_6 hitungan = new No_6();
        System.out.println("Hitung Luas Persegi");
        System.out.println("Hasil : "+hitungan.bangunRuang(5));
        System.out.println("Hitung Luas Persegi Panjang");
        System.out.println("Hasil : "+hitungan.bangunRuang(4,5));
        System.out.println("Hitung Luas Segitiga");
        System.out.println("Hasil : "+hitungan.bangunRuang(5.6, 5.7));
    }
}
