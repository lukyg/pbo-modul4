package No_1;

public class No_1 {

    public void dataDiri(String nama){
        System.out.println("Nama : "+nama);
    }

    public void dataDiri(String nim, String kelompok){
        System.out.println("Nim : "+nim);
        System.out.println("Kelompok : "+kelompok);
    }

    public void dataDiri(int jumlahAnggota){
        System.out.println("Jumlah anggota : "+jumlahAnggota);
    }

    public static void main(String[] args) {
        No_1 data = new No_1();
        data.dataDiri("Ade Safarudin Madani");
        data.dataDiri("F1B019005","5");
        data.dataDiri(4);
    }
}
