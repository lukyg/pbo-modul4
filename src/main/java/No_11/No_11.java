package No_11;

public class No_11 {

    public static void main(String args1) {
        System.out.println("Nama : "+args1);
    }

    public static  void main(String args2, Integer args3){
        System.out.println("NIM : "+args2);
        System.out.println("Kelompok : "+args3);
    }

    public static  void main(String args4, String args5){
        System.out.println("Percobaan : "+args4);
        System.out.println("Banyak anggota : "+args5);
    }

    public static void main(String[] args) {
        No_11.main("Ade Safarudin Madani");
        No_11.main("F1B019005", 5);
        No_11.main("4", "4");

    }
}
