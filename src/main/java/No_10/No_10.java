package No_10;

public class No_10 {

    private static int kuadrat(int nilai){
        return nilai*nilai;
    }

    private static double maksimum(double angka1, double angka2){
        if(angka1<angka2) return angka1;
        else return angka2;
    }

    private static double minimum (double angka3, double angka4){
        if(angka3>angka4) return angka3;
        else return angka4;
    }

    public static void main(String[] args) {
        System.out.println("Hasil kuadrat : "+No_10.kuadrat(10));
        System.out.println("Nilai maksimum : "+No_10.maksimum(3,5));
        System.out.println("Nilai minimum : "+No_10.minimum(4,2));
    }
}
