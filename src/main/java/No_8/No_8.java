package No_8;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class No_8 {

    NumberFormat format = new DecimalFormat("#0.00");
    public String coba(String nama){
        return nama;
    }

    public int coba(int angka1, int angka2){
        return angka1+angka2;
    }

    public String coba(double angka3, double angka4){
        return format.format(angka3/angka4);
    }

    public static void main(String[] args) {
        No_8 testing = new No_8();
        System.out.println("Nama : "+testing.coba("Ade Safarudin Madani"));
        System.out.println("Hasil coba jumlah angka : "+testing.coba(5,6));
        System.out.println("Hasil coba pembagian angka : "+testing.coba(5.6, 7.8));
    }
}
