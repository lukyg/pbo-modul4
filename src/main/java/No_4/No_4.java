package No_4;

public class No_4 {

    public void modul4(String nama, String nim){
        System.out.println("Nama : "+nama);
        System.out.println("NIM : "+nim);
    }

    public void modul4(int kelompok, String judulPraktikum, String judulPercobaan){
        System.out.println("Kelompok : "+kelompok);
        System.out.println("Judul Praktikum : "+judulPraktikum);
        System.out.println("Judul Percobaan : "+judulPercobaan);
    }

    public void modul4(int jumlahAnggota){
        System.out.println("Jumlah anggota : "+jumlahAnggota);
    }

    public void modul4(String namaKetua){
        System.out.println("Nama Ketua : "+namaKetua);
    }

    public static void main(String[] args) {
        No_4 dataIsi = new No_4();
        dataIsi.modul4("Ahmad Rosyid Gege");
        dataIsi.modul4("Ade Safarudin Madani","F1B019005");
        dataIsi.modul4(5, "Pemrograman Berorientasi Objek", "Overloading");
        dataIsi.modul4(4);
    }
}
